# QIF Parser

The library parses QIF files into transaction objects for delectation.

```
<?php
/**
 * General usage.
 */
$parser = new JasonStanley\QIF\Parser();
$results = $parser->parseString($somestring);
$results = $parser->parseFile('./path/to/file.qif');

/**
 * Check if the thing parsed is valid.
 */
$results->isValid();

/**
 * Get the parsed transactions.
 */
$transactions = $results->getComplete();
$transactions[0]->getAmount(); // -1.23
$transactions[0]->getPayee(); // SOME LONG PAYEE STRING £1.00
$transactions[0]->getDate(); // 10/10/2018

/**
 * If validation failed you can see why as follows.
 * Incomplete returns an array of transactions which couldn't be parsed along with an error.
 */
$failed = $results->getIncomplete();
$failed[0]->getErrors();
````

A couple of notes. 

`$transaction->getDate()` doesn't return a datetime object. The main reason for this I do not know the date format and I do not see you could without analysing the file. From what I can tell some QIF files have dd/mm/yy or dd/mm/yyyy or mm/dd/yy etc but no standardised format.

`$transaction->getAmount()` doesn't return a money object. I toyed with allowing a developer to specify a factory to create a Money object for transactions. I may return to that. 

# Roadmap
1.0
* Add amount validation to ensure the number is a float
* Add a money factory to allow a developer to specify the construction of a money object
* Create a helper to work with PAYEE lines