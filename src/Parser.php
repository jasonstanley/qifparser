<?php

namespace JasonStanley\QIF;

use JasonStanley\QIF\Exceptions\CannotOpenFile;
use JasonStanley\QIF\Results\Results;
use JasonStanley\QIF\Results\TransactionError;

/**
 * Class Parser
 * @package JasonStanley\QIF
 *
 * Parse a QIF file. This can be done in one of two options.
 *
 * 1. parseString($string);
 * This will parse from a string and will typically have higher memory usage as the
 * entire string will in memory when parsing.
 *
 * 2. parseFile($path);
 * This will open a file from the given parse the file line by line. This will typically
 * have a lower memory usage.
 */
class Parser
{

	/**
	 * Parse a QIF string.
	 *
	 * @param string $string
	 * @return Results
	 */
	public function parseString(string $string): Results
	{
		$lines = explode(PHP_EOL, $string);

		$results = new Results();
		foreach ($lines as $line) {
			$this->parseLine($results, $line);
		}

		return $results->finished();
	}

	/**
	 * Parse a QIF file.
	 *
	 * @param string $filePath
	 * @return Results
	 * @throws CannotOpenFile
	 */
	public function parseFile(string $filePath): Results
	{
		if (!$handle = fopen($filePath, "r")) {
			throw new CannotOpenFile("Cannot open supplied file.");
		}

		$results = new Results();
		while (($line = fgets($handle)) !== false) {
			$line = str_replace(array("\n", "\r"), '', $line);
			$this->parseLine($results, $line);
		}

		fclose($handle);
		return $results->finished();
	}

	/**
	 * Parse a single line.
	 *
	 * @param Results $results
	 * @param string $line
	 */
	private function parseLine(Results $results, string $line)
	{
		$firstCharacter = substr($line, 0, 1);
		switch ($firstCharacter) {
			case '!':
				$results->setHeader(substr($line, 6));
				break;
			case 'D':
				$results->latest()->setDate(substr($line, 1));
				break;
			case 'T':
				$results->latest()->setAmount(substr($line, 1));
				break;
			case 'P':
				$results->latest()->setPayee(substr($line, 1));
				break;
			case '^':
				$results->store();
				break;
			default:
				$results->latest()->setError(
					new TransactionError(TransactionError::ERR_UNRECOGNISED_LINE, $line)
				);
		}

	}

}