<?php

namespace JasonStanley\QIF\Results;

/**
 * Class Results
 * @package JasonStanley\QIF\Results
 *
 * The Results class holds the result of the parsing.
 *
 * The current transaction will be held in the latest() method. When the latest transaction
 * is complete you can call store() which will validate and store the transaction, then
 * dispense a new transaction object which can be accessed via the latest() method.
 *
 * Example usage.
 *
 * $results = new Results();
 * $results->latest()->setPayee("Some Line");
 * $results->store();
 *
 * $results->isValid(); // Returns if the file was valid.
 * $results->getComplete(); // Returns valid transactions.
 * $results->getIncomplete(); // Returns any incorrectly parsed objects.
 */
class Results
{

	/**
	 * Header String
	 *
	 * @var string
	 */
	private $header = '';

	/**
	 * The latest transaction being modified.
	 *
	 * @var Transaction
	 */
	private $latest;

	/**
	 * List of complete and correct transactions.
	 *
	 * @var Transaction[]
	 */
	private $complete = [];

	/**
	 * List of incomplete and incorrect transactions.
	 *
	 * @var Transaction[]
	 */
	private $incomplete = [];

	public function __construct()
	{
		$this->latest = new Transaction();
	}

	/**
	 * Returns the header string of the file.
	 *
	 * @return string
	 */
	public function getHeader(): string
	{
		return $this->header;
	}

	/**
	 * Set the header string from the qif file.
	 *
	 * @param string $header
	 */
	public function setHeader(string $header)
	{
		$this->header = $header;
	}

	/**
	 * Returns correct and complete transactions.
	 *
	 * @return Transaction[]
	 */
	public function getComplete(): array
	{
		return $this->complete;
	}

	/**
	 * Returns incomplete and incorrect transactions.
	 *
	 * @return Transaction[]
	 */
	public function getIncomplete(): array
	{
		return $this->incomplete;
	}

	/**
	 * Denotes that parsing is finished.
	 * As transactions are only stored when a ^ is hit, this method will flush
	 * any partial transactions from the end of a file.
	 *
	 * @return Results
	 */
	public function finished(): Results
	{
		if (!$this->latest()->isEmpty()) {
			$this->store();
		}

		return $this;
	}

	/**
	 * Returns the current transaction which is being processed.
	 *
	 * @return Transaction
	 */
	public function latest(): Transaction
	{
		return $this->latest;
	}

	/**
	 * Validate and store the latest transaction.
	 * Once complete, create a new transaction and set it as latest()
	 */
	public function store()
	{
		if ($this->latest->isValid()) {
			$this->complete[] = $this->latest;
		} else {
			$this->incomplete[] = $this->latest;
		}
		$this->latest = new Transaction();
	}


	/**
	 * Determine if the results are valid. A file is valid if there is a header
	 * and no incomplete transactions.
	 *
	 * @return bool
	 */
	public function isValid() {
		return $this->header && count($this->incomplete) === 0;
	}

}