<?php

namespace JasonStanley\QIF\Results;

/**
 * Class Transaction
 * @package JasonStanley\QIF\Results
 *
 * There are two things of note:
 *
 * 1. Transactions can store errors. If a line cannot be parsed, it will be set against
 * the transaction as an error. The error will have a type and a string. The string
 * will typically be the line which could not be parsed.
 *
 * 2. Transaction fields can only be set once. Calling setAmount() twice will result
 * in an error being added to the transaction object.
 */
class Transaction
{

	private $amount = '';

	private $date = '';

	private $payee = '';

	/**
	 * @var TransactionError[]
	 */
	private $errors = [];

	/**
	 * @param TransactionError $error
	 */
	public function setError(TransactionError $error)
	{
		$this->errors[] = $error;
	}

	/**
	 * @return array
	 */
	public function getErrors(): array
	{
		return $this->errors;
	}

	/**
	 * Determine if the transaction is valid.
	 *
	 * @return bool
	 */
	public function isValid()
	{
		return $this->getPayee() &&
			$this->getDate() &&
			$this->getAmount() &&
			count($this->errors) === 0;
	}

	/**
	 * @return mixed
	 */
	public function getPayee()
	{
		return $this->payee;
	}

	/**
	 * @param mixed $payee
	 */
	public function setPayee($payee)
	{
		if ($this->payee) {
			$this->setError(new TransactionError(TransactionError::ERR_DUPLICATE_LINE, $payee));
		}

		$this->payee = $payee;
	}

	/**
	 * @return mixed
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param mixed $date
	 */
	public function setDate($date)
	{
		if ($this->date) {
			$this->setError(new TransactionError(TransactionError::ERR_DUPLICATE_LINE, $date));
		}

		$this->date = $date;
	}

	/**
	 * @return mixed
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param mixed $amount
	 */
	public function setAmount($amount)
	{
		if ($this->amount) {
			$this->setError(new TransactionError(TransactionError::ERR_DUPLICATE_LINE, $amount));
		}

		$this->amount = $amount;
	}

	/**
	 * Helper to determine if any of the fields are incomplete.
	 *
	 * @return bool
	 */
	public function isEmpty()
	{
		return $this->getPayee() === '' &&
			$this->getDate() === '' &&
			$this->amount === '';
	}

}