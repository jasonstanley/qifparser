<?php


namespace JasonStanley\QIF\Results;

/**
 * Class TransactionError
 * @package JasonStanley\QIF\Results
 *
 * Object to hold parsing errors for a given transaction.
 */
class TransactionError
{

	const ERR_UNRECOGNISED_LINE = 'UNRECOGNISED_LINE';
	const ERR_DUPLICATE_LINE = 'DUPLICATE_LINE';

	private $line = '';

	private $reason = '';

	public function __construct(string $reason, string $line)
	{
		$this->reason = $reason;
		$this->line = $line;
	}

	/**
	 * @return string
	 */
	public function getLine(): string
	{
		return $this->line;
	}

	/**
	 * @return string
	 */
	public function getReason(): string
	{
		return $this->reason;
	}

}