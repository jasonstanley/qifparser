<?php

namespace Tests;

use JasonStanley\QIF\Parser;
use JasonStanley\QIF\Results\Results;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{

	public function testTheHeaderCanBeParsed()
	{
		foreach ($this->runFile(__DIR__ . '/data/card_payment.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertEquals("Oth L", $results->getHeader());
		}
	}

	/**
	 * The parser has two methods which should produce identical results.
	 * Each test will execute both the parseString and parseFile methods.
	 *
	 * @param string $filePath
	 * @return array
	 */
	private function runFile(string $filePath)
	{
		$resultSets = [];

		$parser = new Parser();
		$resultSets['string'] = $parser->parseString(file_get_contents($filePath));
		$resultSets['file'] = $parser->parseFile($filePath);

		return $resultSets;
	}

	public function testAValidTransactionCanBeParsed()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/card_payment.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(1, $results->getComplete());

			$transaction = $results->getComplete()[0];
			$this->assertEquals('19/07/2016', $transaction->getDate());
			$this->assertEquals('-5.58', $transaction->getAmount());
			$this->assertEquals('CARD PAYMENT TO MCDONALDS,5.58 GBP, RATE 1.00/GBP ON 17-07-2016, 5.58', $transaction->getPayee());
			$this->assertTrue($results->isValid());
		}
	}

	public function testMultipleTransactionsCanBeParsed()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/two_transactions.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(2, $results->getComplete());

			$transaction = $results->getComplete()[0];
			$this->assertEquals('14/04/2015', $transaction->getDate());
			$this->assertEquals('-136.16', $transaction->getAmount());
			$this->assertEquals('CARD PAYMENT TO TESCO STORES 3009,136.16 GBP, RATE 1.00/GBP ON 11-04-2015, 136.16', $transaction->getPayee());

			$transaction = $results->getComplete()[1];
			$this->assertEquals('13/04/2015', $transaction->getDate());
			$this->assertEquals('-60.61', $transaction->getAmount());
			$this->assertEquals('DIRECT DEBIT PAYMENT TO VIRGIN MEDIA PYMTS REF 720993902001, MANDATE NO 0012, 60.61', $transaction->getPayee());
			$this->assertTrue($results->isValid());
		}
	}

	public function testAnEmptyStringCanBeHandled()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/empty.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(0, $results->getComplete());
			$this->assertCount(0, $results->getIncomplete());
			$this->assertFalse($results->isValid());
		}
	}

	public function testAPartialFileIsHandled()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/incomplete.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(0, $results->getComplete());
			$this->assertCount(1, $results->getIncomplete());

			$incomplete = $results->getIncomplete()[0];
			$this->assertEquals('19/07/2016', $incomplete->getDate());
			$this->assertEquals('-5.58', $incomplete->getAmount());
			$this->assertFalse($results->isValid());
		}
	}

	public function testATransactionMissingFinalMarkerIsHandled()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/incomplete_missing_marker.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(0, $results->getComplete());
			$this->assertCount(1, $results->getIncomplete());
			$this->assertFalse($results->isValid());
		}
	}

	public function testAFileWithBadInputIsHandled()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/two_transactions_part_incorrect.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(1, $results->getComplete());
			$this->assertCount(1, $results->getIncomplete());
			$this->assertFalse($results->isValid());
		}
	}

	public function testAFileWithDuplicateDateLinesFails()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/duplicate_date_line.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(0, $results->getComplete());
			$this->assertCount(1, $results->getIncomplete());
			$this->assertFalse($results->isValid());
		}
	}

	public function testAFileWithDuplicateTransactionLinesFails()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/duplicate_transaction_line.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(0, $results->getComplete());
			$this->assertCount(1, $results->getIncomplete());
			$this->assertFalse($results->isValid());
		}
	}

	public function testAFileWithDuplicatePayeeLinesFails()
	{
		/**
		 * @var string $type
		 * @var Results $results
		 */
		foreach ($this->runFile(__DIR__ . '/data/duplicate_payee_line.txt') as $type => $results) {
			$this->assertInstanceOf(Results::class, $results);
			$this->assertCount(0, $results->getComplete());
			$this->assertCount(1, $results->getIncomplete());
			$this->assertFalse($results->isValid());
		}
	}

}